﻿using Unity.Burst;
using Unity.Mathematics;

namespace SynthQuest.Synthesizer.Oscillators
{
    public class TriangleWaveform : Waveform
    {
        protected override void CompileAmplitudeFunction()
        {
            AmplitudeFunction = BurstCompiler.CompileFunctionPointer<Amplitude>(Triangle.Amplitude);
        }
    }

    [BurstCompile]
    internal class Triangle
    {
        [BurstCompile(CompileSynchronously = true)]
        internal static float Amplitude(float phase)
        {
            return 2f * math.abs(1f - 2f * phase) - 1f;
        }
    }
}
