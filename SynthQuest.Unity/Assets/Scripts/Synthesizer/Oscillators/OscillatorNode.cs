﻿using Unity.Audio;
using Unity.Burst;
using Unity.Mathematics;

namespace SynthQuest.Synthesizer.Oscillators
{
    [BurstCompile]
    public struct OscillatorNode : IAudioKernel<OscillatorNode.Parameters, OscillatorNode.Providers>
    {
        public enum Parameters
        {
            [ParameterDefault(440f)]
            [ParameterRange(MIDI.MIN_FREQUENCY, MIDI.MAX_FREQUENCY)]
            Frequency,

            [ParameterDefault(0.5f)]
            [ParameterRange(0f, 1f)]
            LinearGain
        }

        public enum Providers { }

        public FunctionPointer<Waveform.Amplitude> Amplitude;

        public bool IsPlaying;

        float phase;

        public void Initialize() { }

        public void Execute(ref ExecuteContext<Parameters, Providers> context)
        {
            if (!IsPlaying)
                return;

            if (context.Outputs.Count == 0)
                return;

            var outputBuffer = context.Outputs.GetSampleBuffer(0).Buffer;
            var outputChannels = context.Outputs.GetSampleBuffer(0).Channels;

            var inputCount = context.Inputs.Count;
            for (var i = 0; i < inputCount; i++)
            {
                var inputBuffer = context.Inputs.GetSampleBuffer(i).Buffer;
                for (var s = 0; s < outputBuffer.Length; s++)
                    outputBuffer[s] += inputBuffer[s];
            }

            var frames = outputBuffer.Length / outputChannels;
            var sampleRate = context.SampleRate;
            var parameters = context.Parameters;
            for (int s = 0, i = 0; s < frames; s++)
            {
                var amplitude = Amplitude.Invoke(phase);
                var linearGain = parameters.GetFloat(Parameters.LinearGain, s);
                for (var c = 0; c < outputChannels; c++)
                {
                    outputBuffer[i++] += linearGain * amplitude;
                }
                var delta = parameters.GetFloat(Parameters.Frequency, s) / sampleRate;
                phase += delta;
                phase -= math.floor(phase);
            }
        }

        public void Dispose() { }

        [BurstCompile]
        public struct SetWaveform : IAudioKernelUpdate<Parameters, Providers, OscillatorNode>
        {
            FunctionPointer<Waveform.Amplitude> amplitude;

            public SetWaveform(FunctionPointer<Waveform.Amplitude> amplitude)
            {
                this.amplitude = amplitude;
            }

            public void Update(ref OscillatorNode audioKernel)
            {
                audioKernel.Amplitude = amplitude;
            }
        }

        [BurstCompile]
        public struct Play : IAudioKernelUpdate<Parameters, Providers, OscillatorNode>
        {
            public void Update(ref OscillatorNode audioKernel)
            {
                audioKernel.IsPlaying = true;
            }
        }

        [BurstCompile]
        public struct Stop : IAudioKernelUpdate<Parameters, Providers, OscillatorNode>
        {
            public void Update(ref OscillatorNode audioKernel)
            {
                audioKernel.IsPlaying = false;
            }
        }
    }
}
