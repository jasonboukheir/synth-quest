﻿using Unity.Burst;
using Unity.Mathematics;

namespace SynthQuest.Synthesizer.Oscillators
{
    public class SineWaveform : Waveform
    {
        protected override void CompileAmplitudeFunction()
        {
            AmplitudeFunction = BurstCompiler.CompileFunctionPointer<Amplitude>(Sine.Amplitude);
        }
    }

    [BurstCompile]
    internal class Sine
    {
        internal const float DOUBLE_PI = math.PI * 2f;

        [BurstCompile(CompileSynchronously = true)]
        internal static float Amplitude(float phase)
        {
            return math.sin(phase * DOUBLE_PI);
        }
    }
}
