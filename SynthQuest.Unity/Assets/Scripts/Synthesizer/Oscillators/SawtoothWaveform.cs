﻿using Unity.Burst;

namespace SynthQuest.Synthesizer.Oscillators
{
    public class SawtoothWaveform : Waveform
    {
        protected override void CompileAmplitudeFunction()
        {
            AmplitudeFunction = BurstCompiler.CompileFunctionPointer<Amplitude>(Sawtooth.Amplitude);
        }
    }

    [BurstCompile]
    internal class Sawtooth
    {
        [BurstCompile(CompileSynchronously = true)]
        internal static float Amplitude(float phase)
        {
            return 2f * phase - 1f;
        }
    }
}
