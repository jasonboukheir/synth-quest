﻿using Unity.Burst;
using Unity.Mathematics;

namespace SynthQuest.Synthesizer.Oscillators
{
    public class SquareWaveform : Waveform
    {
        protected override void CompileAmplitudeFunction()
        {
            AmplitudeFunction = BurstCompiler.CompileFunctionPointer<Amplitude>(Square.Amplitude);
        }
    }

    [BurstCompile]
    internal class Square
    {
        [BurstCompile(CompileSynchronously = true)]
        internal static float Amplitude(float phase)
        {
            return 2f * math.round(phase) - 1f;
        }
    }
}
