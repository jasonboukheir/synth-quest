﻿using Unity.Burst;
using UnityEngine;

namespace SynthQuest.Synthesizer.Oscillators
{
    public abstract class Waveform : MonoBehaviour
    {
        public delegate float Amplitude(float phase);

        public FunctionPointer<Amplitude> AmplitudeFunction { get; protected set; }

        protected abstract void CompileAmplitudeFunction();

        void Awake()
        {
            CompileAmplitudeFunction();
        }
    }
}
