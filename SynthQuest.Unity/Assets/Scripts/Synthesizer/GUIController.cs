﻿using UnityEngine;

namespace SynthQuest.Synthesizer
{
    public class GUIController : MonoBehaviour
    {
        private const float DELTA = 0.01f;

        [SerializeField]
        Synthesizer synth;

        void OnGUI()
        {
            if (synth)
            {
                OnSynthGUI();
            }
        }

        void OnSynthGUI()
        {
            GUI.color = Color.white;

            GUI.Label(new Rect(100f, 70f, 300f, 30f), "Frequency:");
            var newFrequency = GUI.HorizontalSlider(
                new Rect(100f, 100f, 300f, 30f),
                synth.Frequency,
                MIDI.MIN_FREQUENCY,
                MIDI.MAX_FREQUENCY
            );
            if (AreDifferent(newFrequency, synth.Frequency))
            {
                synth.SetFrequency(newFrequency);
            }

            GUI.Label(new Rect(100f, 160f, 300f, 30f), "Gain:");
            var newGain = GUI.HorizontalSlider(
                new Rect(100f, 190f, 300f, 30f),
                synth.Gain,
                0f,
                1f
            );
            if (AreDifferent(newGain, synth.Gain))
            {
                synth.SetGain(newGain);
            }

            if (GUI.Button(new Rect(100f, 250f, 300f, 30f), "Play"))
            {
                synth.Play();
            }

            if (GUI.Button(new Rect(100f, 280f, 300f, 30f), "Stop"))
            {
                synth.Stop();
            }
        }

        bool AreDifferent(float a, float b)
        {
            return Mathf.Abs(a - b) > DELTA;
        }
    }
}
