﻿namespace SynthQuest.Synthesizer
{
    public static class MIDI
    {
        public const float MAX_FREQUENCY = 4186f;

        public const float MIN_FREQUENCY = 27.5f;
    }
}
