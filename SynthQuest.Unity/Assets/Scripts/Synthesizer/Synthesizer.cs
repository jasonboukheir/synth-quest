﻿using System;
using SynthQuest.Synthesizer.Oscillators;
using Unity.Audio;
using UnityEngine;

namespace SynthQuest.Synthesizer
{
    public class Synthesizer : MonoBehaviour
    {
        [SerializeField]
        Waveform[] waveforms;

        AudioOutputHandle output;
        DSPGraph graph;
        DSPNode oscillatorNode;
        DSPConnection connection;

        public Waveform[] Waveforms { get => waveforms; set => waveforms = value; }

        public float Gain { get; private set; } = 0.5f;

        public float Frequency { get; private set; } = 440f;

        public void Play()
        {
            using (var block = graph.CreateCommandBlock())
            {
                block.UpdateAudioKernel<OscillatorNode.Play,
                    OscillatorNode.Parameters,
                    OscillatorNode.Providers,
                    OscillatorNode>(new OscillatorNode.Play(), oscillatorNode);
            }
        }

        public void Stop()
        {
            using (var block = graph.CreateCommandBlock())
            {
                block.UpdateAudioKernel<OscillatorNode.Stop,
                    OscillatorNode.Parameters,
                    OscillatorNode.Providers,
                    OscillatorNode>(new OscillatorNode.Stop(), oscillatorNode);
            }
        }

        public void SetWaveform(int index)
        {
            using (var block = graph.CreateCommandBlock())
            {
                block.UpdateAudioKernel<
                    OscillatorNode.SetWaveform,
                    OscillatorNode.Parameters,
                    OscillatorNode.Providers,
                    OscillatorNode>(new OscillatorNode.SetWaveform(Waveforms[index].AmplitudeFunction), oscillatorNode);
            }
        }

        public void SetFrequency(float frequency)
        {
            Frequency = frequency;
            using (var block = graph.CreateCommandBlock())
            {
                block.SetFloat<OscillatorNode.Parameters, OscillatorNode.Providers, OscillatorNode>(
                    oscillatorNode, OscillatorNode.Parameters.Frequency, Frequency);
            }
        }

        public void SetGain(float gain)
        {
            Gain = gain;
            using (var block = graph.CreateCommandBlock())
            {
                block.SetFloat<OscillatorNode.Parameters, OscillatorNode.Providers, OscillatorNode>(
                    oscillatorNode, OscillatorNode.Parameters.LinearGain, Gain);
            }
        }

        void Start()
        {
            var format = ChannelEnumConverter.GetSoundFormatFromSpeakerMode(AudioSettings.speakerMode);
            var channels = ChannelEnumConverter.GetChannelCountFromSoundFormat(format);
            AudioSettings.GetDSPBufferSize(out int bufferLength, out int numBuffers);
            var sampleRate = AudioSettings.outputSampleRate;

            graph = DSPGraph.Create(format, channels, bufferLength, sampleRate);

            var driver = new DefaultDSPGraphDriver { Graph = graph };
            output = driver.AttachToDefaultOutput();

            using (var block = graph.CreateCommandBlock())
            {
                oscillatorNode = block.CreateDSPNode<OscillatorNode.Parameters, OscillatorNode.Providers, OscillatorNode>();
                block.AddOutletPort(oscillatorNode, 2, SoundFormat.Stereo);

                connection = block.Connect(oscillatorNode, 0, graph.RootDSP, 0);
            }
            SetWaveform(0);
        }

        void Update()
        {
            graph.Update();
        }

        void OnDisable()
        {
            using (var block = graph.CreateCommandBlock())
            {
                block.Disconnect(connection);
                block.ReleaseDSPNode(oscillatorNode);
            }

            output.Dispose();
        }
    }
}
